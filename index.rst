.. SPDX-License-Identifier: CC-BY-SA-4.0

=============================================
Application usage of media controller devices
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   mc-v4l2-api
   configuring-pipelines

